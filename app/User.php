<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Overtrue\LaravelFollow\Traits\CanFollow;
use Overtrue\LaravelFollow\Traits\CanBeFollowed;

class User extends Authenticatable
{
    use Notifiable, CanFollow, CanBeFollowed;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comments()
    {
        return $this->hasMany('\App\Comment');
    }
   
   public function likes()
   {
       return $this->hasMany('App\like');
   }

   /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
   public function followers()
   {
       return $this->belongsToMany(User::class, 'followers', 'leader_id', 'follower_id')->withTimestamps();
   }

   /**
    * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
    */
   public function followings()
   {
       return $this->belongsToMany(User::class, 'followers', 'follower_id', 'leader_id')->withTimestamps();
   }






}


