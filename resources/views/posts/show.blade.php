@extends('layout.master')

@section('content')

<section>

	<div class="fh5co-about animate-box">
		<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
			<h2>{{ $post->title }}</h2>			
		</div>	

		
		<div class="container" style="margin-bottom: 10px;">
			<div class="col-md-8 col-md-offset-2 animate-box">				
				<p class="pull-left">{{ $post->body }}</p>
			</div>
		</div>

		<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
	
			<h3>Comments</h3>	
						
		</div>	

		
		<div class="container" style="margin-bottom: 10px;">
			@foreach($comments as $comment)

			<div class="col-md-8 col-md-offset-2 animate-box">
				<p class="pull-right">Author: {{ $comment->user->name }}</p>
				<p class="pull-left">{{ $comment->body }}</p>
			</div>

			@endforeach
		</div>


		@guest
		<p>Please log in to comment</p>
		@else
		<div class="container" style="margin-bottom: 10px;">
			<div class="col-md-8 col-md-offset-2 animate-box">				
				<form action="{{ url('/comment') }}" method="post">
					@csrf
					<input type="hidden" name="post_id" value="{{ $post->id }}">	
					<textarea name="body"></textarea>
					<button type="submit">Submit Comment</button>
				</form>
			</div>
		</div>
		@endguest
		

	</div> 
	
	
</section>

@endsection