<!DOCTYPE html>
<html lang="en">
  <head>
    @include('layout.header')
  </head>
  <title>@yield('title','TWEETER')</title>
  <body>
	
    <!-- Static navbar -->
    @include('layout.forlogin_nav')

	@include('layout.headerwrap')
	
	<section id="works"></section>
	@yield('content')
	
	<br>

	@include('layout.social')

	@include('layout.footer')

	@include('layout.scripts')


  </body>
</html>
