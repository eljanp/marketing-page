@extends('layout.forlogin')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                  
                    <h3 class="card-header">Congratulations!</h3>

                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif

                        <p>You are now tweeter user.</p> 
                        <p>Welcome, {{ Auth::user()->name }}</p>

                        <form action="{{ url('/logout') }}" method="post">
                            @csrf
                            <button class="navbar-brand btn btn-primary " type="submit">Log Out</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
@endsection
