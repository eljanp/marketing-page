<div class="header" id="myHeader">
	<nav>
	@guest	
	<ul class="left nav nav-pills mb-3" id="pills-tab" role="tablist">
	
		  <li class="nav-item">
		    <a class="nav-link active" id="pills-home-tab"  href="{{ url('/login')}}" 
		    role="tab"  aria-controls="pills-home" aria-selected="true">LOGIN</a>
		  </li>
		  <li class="nav-item">
		    <a class="nav-link" id="pills-profile-tab" href="{{ url('/register') }}" role="tab" aria-controls="pills-profile" aria-selected="false">SIGN UP</a>
		  </li>
	</ul>	
	@endguest

	<script type="text/javascript">
	 window.onscroll = function() {myFunction()};

	 var header = document.getElementById("myHeader");
	 var sticky = header.offsetTop;

	 function myFunction() {
	   if (window.pageYOffset > sticky) {
	     header.classList.add("sticky");
	   } else {
	     header.classList.remove("sticky");
	   }
	 }
	</script>

   <div class="logo-image">
      <a  href="{{url ('/')}}">
       <img src="images/logo.png" alt="{{url ('/')}}">
     </a>
   </div>
   <div>
     <form action="http://search.yahoo.com/search" method="GET">
        <input type="text" name="search" placeholder="Search..">
     </form>
    </div>   
    
	</nav>

</div>