<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ url('/') }}">TWEETER</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
      @guest
        <li class="active"><a href="{{ url('/login') }}">Login</a></li>
        <li><a href="{{ url('/register') }}">Register</a></li>
       @else
        <center>
          <div style="border: 1px solid grey;
                      padding:40px;
                      display: table;
                        margin-top: 100px;
                        margin-right: auto;
                        margin-left: auto;
                      ">   
            <p class="navbar-brand" style="color:white;">Are you sure you want to logout?</p>
            
            <form action="{{ url('/logout') }}" method="post">
                  @csrf
            <button class="navbar-brand btn btn-primary" type="radio" style="color:white" >Yes</button>
            </form>
                      
            <button class="navbar-brand btn btn-primary pull-right" ><a style="
              text-decoration: none;
              color: white;
              " href="{{ url('/tweet') }}">No</a>
            </button>
             
             @endguest
            </div>
          </center> 
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>




