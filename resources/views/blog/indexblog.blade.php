<!DOCTYPE html>
<html>
<head>
	
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
	<meta charset="utf-8">
	<title>Bootply snippet - Faceboot - A Facebook style template for Bootstrap</title>
	<meta name="generator" content="Bootply" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="description" content="Fixed header with independent scrolling left nav and right content." />
	
	<script src="{{ asset('js/custom.js') }}" defer></script>

	<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.2/css/bootstrap.min.css" rel="stylesheet">
	<link rel="apple-touch-icon" href="/bootstrap/img/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="/bootstrap/img/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="/bootstrap/img/apple-touch-icon-114x114.png">

	@include('blog.css')

	<title>blog</title>

</head>
	<body>
		<div class="wrapper">
		    <div class="box">
		    	<div class="row row-offcanvas row-offcanvas-left">
		            <div class="column col-sm-2 col-xs-1 sidebar-offcanvas" id="sidebar">
  		              	<ul class="nav">
		          			<li><a href="#" data-toggle="offcanvas" class="visible-xs text-center"><i class="glyphicon glyphicon-chevron-right"></i></a></li>
		            	</ul>
		               <p>PEOPLE YOU MAY KNOW "IN TWEETER"</p>
		               <p>
		               	


		               </p>
	              	 <div> 	
	               		@yield('content')
		             </div>
		                <ul class="nav hidden-xs" id="lg-menu">
							{{-- @foreach($users as $user)
							{{ $user->name }}
							@endforeach --}}
		                    <li><a href="#"><i class="glyphicon glyphicon-refresh"></i> Refresh</a></li>
		                </ul>
		              	<ul class="nav visible-xs" id="xs-menu">
		                  	<li><a href="#featured" class="text-center"><i class="glyphicon glyphicon-list-alt"></i></a></li>
		                    <li><a href="#stories" class="text-center"><i class="glyphicon glyphicon-list"></i></a></li>
		                  	<li><a href="#" class="text-center"><i class="glyphicon glyphicon-paperclip"></i></a></li>
		                    <li><a href="#" class="text-center"><i class="glyphicon glyphicon-refresh"></i></a></li>
		                </ul>
  		            </div>
 		            <div class="column col-sm-10 col-xs-11" id="main">
  		              	<div class="navbar navbar-blue navbar-static-top">  
		                    <div class="navbar-header">
		                      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
		                        <span class="sr-only">Toggle</span>
		                        <span class="icon-bar"></span>
		          				<span class="icon-bar"></span>
		          				<span class="icon-bar"></span>
		                      </button>
		                      <a href="/" class="navbar-brand logo">T</a>
		                  	</div>
		                  	<nav class="collapse navbar-collapse" role="navigation">
		                  	  <form class="navbar-form navbar-left">
		                        <div class="input-group input-group-sm" style="max-width:360px;">
		                          <input type="text" class="form-control" placeholder="Search" name="srch-term" id="srch-term">
		                          <div class="input-group-btn">
		                            <button class="btn btn-default" type="submit"><i class="glyphicon glyphicon-search"></i></button>
		                          </div>
		                        </div>
		                   	 </form>
		                    <ul class="nav navbar-nav">
		                      <li>
		                        <a href="{{ url('/') }}"><i class="glyphicon glyphicon-home"></i> Home</a>
		                      </li>
		                      <li>
		                        <a href="#postModal" role="button" data-toggle="modal"><i class="glyphicon glyphicon-plus"></i> Post</a>
		                      </li>
		                      <li>
		                        <a href="#"><span class="badge">{{ Auth::user()->name }}</span></a>
		                      </li>
		                    </ul>
		                    <ul class="nav navbar-nav navbar-right">
		                      <li class="dropdown">
		                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-user"></i></a>
		                        <ul class="dropdown-menu">
		                          <li><a href="">Settings</a></li>
		                          <li><a href="">Terms & Policies</a></li>
		                          <li><a href="">Help Center</a></li>
		                          <li><a href="">Report a Problem</a></li>
		                          <li><a href="{{ url('/') }}">Log Out</a></li>
		                        </ul>
		                      </li>
		                    </ul>
		                  	</nav>
		                </div>
		            		              
		                <div class="padding">
		                    <div class="full col-sm-9"> 
		                      	<div class="row">
 		                          	<div class="col-sm-5">
   		                              <div class="panel panel-default">
		                                <div class="panel-thumbnail"><img src="/assets/img/work.jpg" class="img-responsive"></div>
		                                <div class="panel-body">
		                                  <p class="lead">Welcome! {{ Auth::user()->name }}</p>

		                                  <p>45 Followers, 13 Posts</p>
		                                  
		                                 <img src="https://lh3.googleusercontent.com/uFp_tsTJboUY7kue5XAsGA=s28" width="28px" height="28px">
		                                  </p>
		                                </div>
		                              </div>
		                              <div class="well"> 
		                                   <form method="post" action="{{ url('/post') }}" class="form-horizontal" role="form">
		                                   	@csrf
		                                    <h4>What's New</h4>
		                                     <div class="form-group" style="padding:14px;">
		                                      <textarea class="form-control"  cols="20" rows="7" placeholder="Update your status" name="body">{{ old('body') }} </textarea>
		                                      @if ($errors->has('body'))
		                                          <span class="invalid-feedback" role="alert">
		                                              <strong>{{ $errors->first('body') }}</strong>
		                                          </span>
		                                      @endif
		                                    </div>
		                                    <input class="btn btn-primary pull-right" type="submit">
		                                    <ul class="list-inline"><li><a href=""><i class="glyphicon glyphicon-upload"></i></a></li><li><a href=""><i class="glyphicon glyphicon-camera"></i></a></li><li><a href=""><i class="glyphicon glyphicon-map-marker"></i></a></li></ul>
		                                  </form>
		                              </div>
		                           		 <div class="panel panel-default">
		                           		  <div class="panel-thumbnail"><img src="/assets/img/back.jpg" class="img-responsive"></div>
		                           		  <div class="panel-body">
		                           		    <p class="lead">Social Good</p>
		                           		    <p>1,200 Followers, 83 Posts</p>
		                           		    
		                           		    <p>
		                           		      <img src="https://lh6.googleusercontent.com/-5cTTMHjjnzs/AAAAAAAAAAI/AAAAAAAAAFk/vgza68M4p2s/s28-c-k-no/photo.jpg" width="28px" height="28px">
		                           		      <img src="https://lh4.googleusercontent.com/-6aFMDiaLg5M/AAAAAAAAAAI/AAAAAAAABdM/XjnG8z60Ug0/s28-c-k-no/photo.jpg" width="28px" height="28px">
		                           		      <img src="https://lh4.googleusercontent.com/-9Yw2jNffJlE/AAAAAAAAAAI/AAAAAAAAAAA/u3WcFXvK-g8/s28-c-k-no/photo.jpg" width="28px" height="28px">
		                           		    </p>
		                           		  </div>
		                           		</div>
		                          </div>
		                          <div class="col-sm-7">           
	                                 <div class="panel panel-default">
	                                   <div class="panel-heading"><a href="#" class="pull-right">View all</a> <h4>TWEETER</h4>
	                                   	<br>
	                                   </div>
	                                    <div class="panel-body">
   	                                      @foreach($posts as $post)
	                                 	  @guest
		                                  @else
 		                                  @endguest
		                                    <p >{{ $post->body }}
		                                    <code>by {{ $post->user->name }}</code> |
		                                    <code> <a href="">Follow</a> </code>|
		                                    <code> <a href="">Unfollow</a> </code>
		                                	</p>
		                                	<hr>
		                                	<p>who likes</p>
   		                                	<hr>
			                                <code><a href="">Like<a/></code>|
			                                <code><a href= "{{ url( '/post/'.$post->id.'/edit' ) }}">Edit</a></code>|
			                                <code><a href= "{{ url( '/post/'.$post->id.'/delete' ) }}">Delete</a></code>|
		                                  <hr>
 	                                     <div style="padding: 10px;">
											@foreach($comments as $comment)
											<div style="margin: -20px 50px 50px 50px;" >
												@if($post->id == $comment->post_id)
													<p>{{ $comment->body }}
														<code>by {{ $comment->user->name }}
														</code>|
														<code><a href="">Follow</a>
														</code>|
	                                    				<code><a href="">Unfollow</a>
	                                    				</code>
													</p>
												@endif
		                   					</div>   
											@endforeach
											@guest
											<p>Please log in to comment</p>
											@else	
											@endguest
											<div class="input-group-btn">				
												<form action="{{ url('/comment') }}" method="post">
													@csrf
													<div align ="right" >	
														<input  class="form-control wid100" type="hidden" name="post_id" value="{{ $post->id }}">	
														<textarea rows="1" cols="10" placeholder="Add a comment" class="form-control" name="body">
														</textarea>
														 <button type="submit" class="btn btn-default btn btn-info"><i class="glyphicon glyphicon-share"></i>
														 </button>
													</div>	 
												</form>
											</div>
												@endforeach
	                                  		</div>
   		                                  </div>
   				                          <div class="col-sm-6">
				                            <p><a href="#" class="pull-right">©Copyright 2013</a></p>
				                          </div>
				                   		 </div>
					                </div>

									<div id="postModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
									  <div class="modal-dialog">
										  <div class="modal-content">
										      <div class="modal-header">
										          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
													Update Status
										      </div>
										      <div class="modal-body">
										          <form class="form center-block">
										            <div class="form-group">
										              <textarea class="form-control input-lg" autofocus="" placeholder="What do you want to share?" name="body">{{ old('body') }}</textarea>
										              @if ($errors->has('body'))
										                  <span class="invalid-feedback" role="alert">
										                      <strong>{{ $errors->first('body') }}</strong>
										                  </span>
										              @endif
										            </div>
										          </form>
										      </div>
										      <div class="modal-footer">
										          <div>
										          <button class="btn btn-primary btn-sm" data-dismiss="modal" type="submit" aria-hidden="true">Post</button>
										            <ul class="pull-left list-inline"><li><a href=""><i class="glyphicon glyphicon-upload"></i></a></li><li><a href=""><i class="glyphicon glyphicon-camera"></i></a></li><li><a href=""><i class="glyphicon glyphicon-map-marker"></i></a></li></ul>
												  </div>	
										      </div>
										  	</div>
									 	 </div>
									</div>
								</div>



								
		<script src="node_modules/vue/dist/vue.js"></script>
		<script src="js/app.js"></script>

		<script type='text/javascript' src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <script type='text/javascript' src="//netdna.bootstrapcdn.com/bootstrap/3.0.2/js/bootstrap.min.js"></script>
		        
        <script type='text/javascript'>
		        
		    $(document).ready(function() {
		  	$('[data-toggle=offcanvas]').click(function() {
		  	$(this).toggleClass('visible-xs text-center');
		    $(this).find('i').toggleClass('glyphicon-chevron-right glyphicon-chevron-left');
		    $('.row-offcanvas').toggleClass('active');
		    $('#lg-menu').toggleClass('hidden-xs').toggleClass('visible-xs');
		    $('#xs-menu').toggleClass('visible-xs').toggleClass('hidden-xs');
		    $('#btnShow').toggle();
			});
		        
		        });
		        
		 </script>
		        
        <script>
          (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
          (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
          m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
          })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
          ga('create', 'UA-40413119-1', 'bootply.com');
          ga('send', 'pageview');
        </script>
	</body>
</html>
