<!doctype html>
<html lang="en">
  <head>
    @include('marketing.links')
  	 <title>Marketing</title>
    @include('marketing.css')
  </head>

  <body>
  	@include('marketing.background')

  	<div class="top-container">
      @include('marketing.header')
      @include('marketing.video')
	 </div>
    @include('marketing.sticky')  
    @include('marketing.headline')
    @include('marketing.tweeter1')
    @include('marketing.airplane') 
    @include('marketing.gk-chesterton')
    @include('marketing.article')
    @include('marketing.heart')
    @include('marketing.fader')
    @include('marketing.footer')
    
    @include('marketing.script')
  </body>
</html>