<script src="{{ asset('js/custom.js') }}" defer></script>
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">PEOPLE YOU MAY KNOW
                </div>
                <div class="card-body">
                    <div class="row pl-5">
                        @foreach($users as $user)
                        {{ $user->name }} <br>
                        @endforeach
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
