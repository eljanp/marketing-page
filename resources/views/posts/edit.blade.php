<center>
<div class="fh5co-about animate-box">
	<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
		<h2>Edit Post</h2>
	</div>
	<div class="container" style="margin-bottom: 10px;">
		<div class="col-md-8 col-md-offset-2 animate-box">	
			<div class="row">
				<form method="post" action="{{ url( '/post/'.$post->id ) }}">
					@csrf
					<div class="col-md-12">
						<div class="form-group">
							 <!-- <input class="form-control" placeholder="Title" type="text" name="title" 
							 value="{{ ( old('title') ) ? old('title') : $post->title }}"> -->
							@if ($errors->has('title'))
							    <span class="invalid-feedback" role="alert">
							        <strong>{{ $errors->first('title') }}</strong>
							    </span>
							@endif
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<textarea class="form-control" id="" cols="30" rows="7" placeholder="Post Body" name="body">{{ ( old('body') ) ? old('body') : $post->body }}</textarea>
							@if ($errors->has('body'))
							    <span class="invalid-feedback" role="alert">
							        <strong>{{ $errors->first('body') }}</strong>
							    </span>
							@endif
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input value="Update Post" class="btn btn-primary" type="submit">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</center>


