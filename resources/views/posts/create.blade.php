@extends('layout.master')

@section('content')
<div class="fh5co-about animate-box">
	<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
		<h2>Create Post</h2>
	</div>
	<div class="container" style="margin-bottom: 10px;">
		<div class="col-md-8 col-md-offset-2 animate-box">	
			<div class="row">
				<form method="post" action="{{ url('/post') }}">
					@csrf
					<div class="col-md-12">
						<div class="form-group">
							<textarea class="form-control" id="" cols="30" rows="7" placeholder="Post Body" name="body">{{ old('body') }}</textarea>
							@if ($errors->has('body'))
							    <span class="invalid-feedback" role="alert">
							        <strong>{{ $errors->first('body') }}</strong>
							    </span>
							@endif
						</div>
					</div>
					<div class="col-md-12">
						<div class="form-group">
							<input value="Create Post" class="btn btn-primary" type="submit">
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

@endsection