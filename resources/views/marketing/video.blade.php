<div class="because">
	<video id="myvideo" max-width="100%" autoplay controls>
	  <source class="active" src="video/ICON_VERSION5.mp4" type="video/mp4" />
	  <source src="video/ICON_VERSION8.mp4" type="video.mp4"  />
	  <source src="video/black.mov" type="video.mov"  />
	</video>

	<script type="text/javascript">
	  var myvid = document.getElementById('myvideo');

	  myvid.addEventListener('ended', function(e) {
	    var activesource = document.querySelector("#myvideo source.active");
	    var nextsource = document.querySelector("#myvideo source.active + source") || document.querySelector("#myvideo source:first-child");
	    activesource.className = "";
	    nextsource.className = "active";        
	    myvid.src = nextsource.src;
	    myvid.play();
	  });
	</script>
	 
	  <section class="overlay">

	 	<h2>Your social life begins here!</h2>

	 	<p class="ex2">Fired-up</p>
	 	<p class="ex2">Relationships</p>
	 	<p class="ex2">Integrity</p>
	 	<p class="ex2">Enthusiasm</p>
	 	<p class="ex2">Nobility</p>
	 	<p class="ex2">Docile</p>
	 	<p class="ex2">Systematic</p>
	 	
	 </section>
</div>
