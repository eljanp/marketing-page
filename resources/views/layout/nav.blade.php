<div class="navbar navbar-default navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ url('/') }}">TWEETER</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav navbar-right">
      @guest
        <li class="active"><a href="{{ url('/login') }}">Login</a></li>
        <li><a href="{{ url('/register') }}">Register</a></li>
       @else
       	<li>Welcome, {{ Auth::user()->name }}</li>
       	<li>
       		<form action="{{ url('/logout') }}" method="post">
       		    @csrf
       		    <button class="btn btn-primary" type="submit">Log Out</button>
       		</form>
       	</li>
       @endguest

      </ul>
    </div>
  </div>
</div>
