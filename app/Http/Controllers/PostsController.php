<?php

namespace App\Http\Controllers;

use \App\User;
use Illuminate\Http\Request;
use \App\Post;
use \App\Comment;
use Illuminate\Support\Facades\Auth;
use Validator;

class PostsController extends Controller
{
    public function index()
    {
        $posts = Post::all();
       
        $data['posts'] = $posts;

        return view('posts.index', $data);
        
    }

    public function blog()
    {
		$posts = Post::all()->sortByDesc('created_at');

	    $comments = Comment::all();

		$data['posts'] = $posts;

        $data['comments'] = $comments;
        
		return view('blog.indexblog', $data);
		
    }

    public function show($id)
    {
    	$post = Post::find($id);

        $data['comments'] = $post->comments;

        // dd($post->comments);

    	$data['post'] = $post;

    	return view('posts.show', $data);
    }

    public function create()
    {
    	return view('posts.create');
    }

    public function edit($id)
    {
    	$post = Post::find($id);

    	$data['post'] = $post;

    	return view('posts.edit', $data);
    }

    public function store(Request $request)
    {
    	$validator = Validator::make($request->all(), [
		  'body' => 'required',
		]);

		if ($validator->fails()) {
			return redirect('/post')// ('/post') 
				->withErrors($validator)
				->withInput();
		}

		//this code will only run if validation succeeds
    	$post = new Post;

    	//$post->title = $request->input('title');
    	$post->body = $request->input('body');

        $post->user_id = Auth::user()->id;
    	if ($post->save()) {

    		return redirect('/posts'); //('/posts')

    	} 

    }
    
    public function update(Request $request, $id)
    {
    	$validator = Validator::make($request->all(), [
			//'title' => 'required|max:255',
			'body' => 'required',
		]);

		if ($validator->fails()) {
			return redirect('/post/'.$id.'/edit')
				->withErrors($validator)
				->withInput();
		}

		$post = Post::find($id);

		// $post->title = $request->input('title');
		$post->body = $request->input('body');

		if ($post->save()) {

			return redirect('/tweet');

		}  

    }

    public function storeComment(Request $request)
    {
        $comment = new \App\Comment;

        $comment->user_id = Auth::user()->id;
        $comment->post_id = $request->input('post_id');
        $comment->body = $request->input('body');

        $comment->save();

        return redirect( '/post/'.$request->input('post_id') );
    }
    

    public function delete($id)
        {
           
            Post::find($id)->delete();
            return redirect('/tweet');
            
       
       }
    public function storePost(Request $request)

    {
        $post = new \App\Post;

        $post->user_id = Auth::user()->id;
        // $post->post_id = $request->input('post_id');
        $post->body = $request->input('body');

        return redirect( '/post/'.$request->input('user_id') );
    }
    

    // public function whoLikes($id)
    // {
    //     $user = User::find($id);

    //     $data['users'] = $user;

    //     return view ('likes.wholikes');
        
    // }

}

